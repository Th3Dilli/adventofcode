// reading a text file
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

unsigned long getSlope(int row, int col, int mod, vector<string> data) {
  int coli = col;
  unsigned long count = 0;
  for (int i = row; i < data.size(); i += row) {
    if (data[i][coli % mod] == '#') count++;
    coli += col;
  }
  return count;
}

int main() {
  string line;
  ifstream myfile("day3/data");
  if (myfile.is_open()) {
    vector<string> data;

    while (getline(myfile, line)) {
      data.push_back(line);
    }
    myfile.close();

    int mod = line.length();
    unsigned long i1 = getSlope(1, 3, mod, data);
    unsigned long i2 = getSlope(1, 1, mod, data);
    unsigned long i3 = getSlope(1, 5, mod, data);
    unsigned long i4 = getSlope(1, 7, mod, data);
    unsigned long i5 = getSlope(2, 1, mod, data);
    cout << (i1 * i2 * i3 * i4 * i5) << endl;
  }

  else
    cout << "Unable to open file";

  return 0;
}