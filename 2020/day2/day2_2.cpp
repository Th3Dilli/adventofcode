// reading a text file
#include <fstream>
#include <iostream>
#include <string>
using namespace std;

int main() {
  string line;
  ifstream myfile("day2/data");
  int total = 0;
  if (myfile.is_open()) {
    while (getline(myfile, line)) {
      // cout << line << '\n';
      size_t sep = line.find("-");
      string s1 = line.substr(0, sep);
      int num1 = stoi(s1);
      size_t spa = line.find(" ", sep);
      string s2 = line.substr(sep + 1, spa - (sep + 1));
      int num2 = stoi(s2);

      char rule = line[spa + 1];

      size_t search = spa + 3;

      if ((line[search + num1] == rule) xor (line[search + num2] == rule)) {
        cout << line << endl;
        total++;
      }
    }
    myfile.close();
    cout << total << endl;
  }

  else
    cout << "Unable to open file";

  return 0;
}