// // reading a text file
// #include <fstream>
// #include <iostream>
// #include <string>
// #include <vector>
// using namespace std;

// bool check(string nice, string s) {
//   bool a = (nice.find(s) != string::npos);
//   return a;
// }

// int main() {
//   string line;
//   ifstream myfile("day4/data");

//   string fields[] = {"byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"};

//   if (myfile.is_open()) {
//     vector<string> data;

//     while (getline(myfile, line)) {
//       data.push_back(line);
//     }
//     myfile.close();

//     int count = 0;

//     string nice = "";
//     for (string s : data) {
//       if (s == "") {
//         bool skip = false;
//         for (int i = 0; i < fields->length(); i++) {
//           if (!check(nice, fields[i])) {
//             skip = true;
//             break;
//           }
//         }
//         if (!skip) {
//           count++;
//         }
//         nice = "";
//       } else {
//         nice += " " + s;
//       }
//     }
//     cout << count << endl;
//   }

//   else
//     cout << "Unable to open file";

//   return 0;
// }
#include <cstdlib>
#include <iostream>
#include <map>
#include <regex>
#include <sstream>
using namespace std;

static void add_records_to_map(string line, map<string, string> &map) {
  istringstream stream(line);
  string record;
  while (stream >> record) {
    auto colon_position = record.find(':');
    if (colon_position != string::npos) {
      auto key = record.substr(0, colon_position);
      auto value = record.substr(colon_position + 1);
      map[key] = value;
    }
  }
}

static bool read_credential(istream &in, map<string, string> &credential) {
  bool read_succeeded = false, read_something = false;
  do {
    string line;
    if (getline(in, line) && line != "") {
      read_succeeded = true;
      read_something = true;
      add_records_to_map(line, credential);
    } else {
      read_succeeded = false;
    }
  } while (read_succeeded);
  return read_something;
}

static bool contains_north_pole_credentials(
    const map<string, string> &credential) {
  return credential.find("byr") != credential.end() &&
         credential.find("iyr") != credential.end() &&
         credential.find("eyr") != credential.end() &&
         credential.find("hgt") != credential.end() &&
         credential.find("hcl") != credential.end() &&
         credential.find("ecl") != credential.end() &&
         credential.find("pid") != credential.end();
}

static bool valid_north_pole_credential(const map<string, string> &credential) {
  auto byr = atoi(credential.at("byr").c_str());
  auto iyr = atoi(credential.at("iyr").c_str());
  auto eyr = atoi(credential.at("eyr").c_str());
  auto hgt_in = credential.at("hgt").find("in") != string::npos;
  auto hgt_cm = credential.at("hgt").find("cm") != string::npos;
  auto hgt = atoi(credential.at("hgt").c_str());
  auto hgt_valid = false;
  if (hgt_in)
    hgt_valid = hgt >= 59 && hgt <= 76;
  else if (hgt_cm)
    hgt_valid = hgt >= 150 && hgt <= 193;
  return byr >= 1920 && byr <= 2002 && iyr >= 2010 && iyr <= 2020 &&
         eyr >= 2020 && eyr <= 2030 && hgt_valid &&
         regex_match(credential.at("ecl"),
                     regex("^(amb|blu|brn|gry|grn|hzl|oth)$")) &&
         regex_match(credential.at("pid"), regex("^[0-9]{9}$")) &&
         regex_match(credential.at("hcl"), regex("^#([0-9]|[a-f]){6}$"));
}

int main() {
  map<string, string> credential;
  auto valid_passports = 0;
  auto validated_passports = 0;
  while (read_credential(cin, credential)) {
    if (contains_north_pole_credentials(credential)) {
      valid_passports++;
      if (valid_north_pole_credential(credential)) validated_passports++;
    }
    credential.clear();
  }
  cout << "valid passports: " << valid_passports << endl;
  cout << "validated passports: " << validated_passports << endl;
}