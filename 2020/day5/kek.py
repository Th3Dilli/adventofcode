taken_seats = set()
with open('input.txt', 'r') as file:
    for boarding_pass in file:
        rows, cols = list(range(128)), list(range(8))
        for char in boarding_pass:
            if char == 'F':
                rows = rows[:len(rows) // 2]
            elif char == 'B':
                rows = rows[len(rows) // 2:]
            elif char == 'L':
                cols = cols[:len(cols) // 2]
            else:
                cols = cols[len(cols) // 2:]
        taken_seats.add(rows[0] * 8 + cols[0])

all_seats = set(range(min(taken_seats), max(taken_seats) + 1))
my_seat = list(all_seats - taken_seats)[0]

print(f'The highest seat ID is {max(taken_seats)}.')     # part 1
print(f'My seat ID is {my_seat}.')                       # part 2
