#!/bin/python3

def func(a):
    return a.split("\n")


with open("/home/dilli/Documents/adventofcode/day6/data") as f:
    data = f.read().split('\n\n')

data = list(map(func, data))

answers = []

for group in data:
    grp_ans = set()
    for member in group:
        for answer in member:
            grp_ans.add(answer)
    answers.append(grp_ans)

out = 0
for i in answers:
    out += len(i)
print(out)

answers = []
for group in data:
    grp_ans = set(group[0])
    for member in group:
        grp_ans = grp_ans.intersection(set(list(member)))
    answers.append(grp_ans)

out = 0
for i in answers:
    out += len(i)
print(out)
