namespace Aoc
{
  public abstract class AOCBase
  {
    public abstract void Part1();
    public abstract void Part2();
  }
}