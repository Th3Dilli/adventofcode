
namespace Aoc
{
  public class Day2 : AOCBase
  {
    public class Command
    {
      public string Cmd;
      public int Value;
      public Command(string cmd, int value)
      {
        Cmd = cmd;
        Value = value;
      }
    }
    public IEnumerable<Command> GetData()
    {

      string[] lines = File.ReadAllLines(@"day2/input.txt");

      return lines.Select(line =>
      {
        string[]? split = line.Split(" ");
        return new Command(split[0], int.Parse(split[1]));
      });
    }

    public override void Part1()
    {
      var data = GetData();
      int pos = 0, depth = 0;

      foreach (Command cmd in data)
      {
        switch (cmd.Cmd)
        {
          case "forward":
            {
              pos += cmd.Value;
              break;
            }
          case "down":
            {
              depth += cmd.Value;
              break;
            }
          case "up":
            {
              depth -= cmd.Value;
              break;
            }
        }
      }
      Console.WriteLine($"{pos}:{depth}");
      Console.WriteLine($"{pos * depth}");
    }

    public override void Part2()
    {
      var data = GetData();
      int pos = 0, depth = 0, aim = 0;

      foreach (Command cmd in data)
      {
        switch (cmd.Cmd)
        {
          case "forward":
            {
              pos += cmd.Value;
              depth += aim * cmd.Value;
              break;
            }
          case "down":
            {
              aim += cmd.Value;
              break;
            }
          case "up":
            {
              aim -= cmd.Value;
              break;
            }
        }
      }

      Console.WriteLine($"{pos}:{depth}:{aim}");
      Console.WriteLine($"{pos * depth}");
    }
  }
}