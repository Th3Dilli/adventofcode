namespace Aoc
{
  internal class Day1 : AOCBase
  {
    public override void Part1()
    {

      string[] lines = File.ReadAllLines(@"day1/input.txt");

      List<int>? linesint = lines.Select(line => int.Parse(line)).ToList();

      int num = 0;
      for (int i = 0; i < linesint.Count - 1; i++)
      {

        if (linesint[i] < linesint[i + 1])
        {
          num++;
        }
      }

      Console.WriteLine(num);
    }

    public override void Part2()
    {

      string[] lines = File.ReadAllLines(@"day1/input.txt");

      List<int>? linesint = lines.Select(line => int.Parse(line)).ToList();

      int num = 0;
      for (int i = 0; i < linesint.Count - 3; i++)
      {
        int sum1 = linesint[i] + linesint[i + 1] + linesint[i + 2];
        int sum2 = linesint[i + 1] + linesint[i + 2] + linesint[i + 3];
        if (sum1 < sum2)
        {
          num++;
        }
      }

      Console.WriteLine(num);
    }
  }
}